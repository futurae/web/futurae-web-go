## Overview

**futurae-web-go** - Provides the Futurae Web Go helpers to be integrated in your Go web
application.

These helpers allow developers to integrate Futurae's Web authentication suite into their web apps,
without the need to implement the Futurae Auth API.

## Installation
To use **futurae-web-go**, follow these steps:

* If you're using `go get`, download and install it:
```bash
$ go get gitlab.com/futurae/web/futurae-web-go
```

* If you're using `dep`, add it to the go dependencies:
```bash
$ dep ensure -add gitlab.com/futurae/web/futurae-web-go
```

Once you've installed it, import the package:
```golang
import "gitlab.com/futurae/web/futurae-web-go"
```

## Unit Tests

```bash
$ go test
```

## Run example

Please adjust the config-constants in `server.go` with the appropriate values you can get from your [Futurae Admin-Account](https://admin.futurae.com/):
```
WID      Web ID
WKEY     Web Key
HOST     API hostname
```

Generate the `SKEY` as described in the [Docs](https://www.futurae.com/docs/guide/futurae-web/#generate-your-secret-key).

Then change to the examples directory and run the server.
```
cd example
go run .
```

Point your browser to http://localhost:8080/?username=example and follow the instructions
to add a new device and authenticate the given user.

## Documentation

Documentation on using the Futurae Web Widget can be found [here](https://futurae.com/docs/guide/futurae-web/).
