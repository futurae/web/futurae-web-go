package main

import (
	"fmt"
	"html/template"
	"net/http"

	futuraeweb "gitlab.com/futurae/web/futurae-web-go"
)

const (
	WID           = ""
	WKEY          = ""
	SKEY          = ""
	HOST          = ""
	RESPONSE_URL  = "/response"
	HTML_TEMPLATE = `
	<html>
		<head>
			<style>
				#futurae_widget {
					width: 100%;
					height: 501px;
					border: none;
				}
			</style>
		</head>
		<body>
			<form method="post" id="futurae_form"></form>
			<iframe id="futurae_widget"
							data-host="{{ .Host }}"
							data-sig-request="{{ .SigRequest }}"
							data-post-action="{{ .PostAction }}"
							data-lang="en"
							allow="microphone">
			</iframe>
			<script src="js/Futurae-Web-SDK-v1.js"></script>
		</body>
	</html>
	`
)

func handler(w http.ResponseWriter, r *http.Request) {
	username := r.URL.Query().Get("username")

	sig_request, err := futuraeweb.SignRequest(WID, WKEY, SKEY, username)
	if err != nil {
		fmt.Println(err)
	}

	page := struct {
		Host       string
		SigRequest string
		PostAction string
	}{
		Host:       HOST,
		SigRequest: sig_request,
		PostAction: RESPONSE_URL,
	}

	p, err := template.New("example").Parse(HTML_TEMPLATE)
	if err != nil {
		fmt.Println(err)
	}

	p.ExecuteTemplate(w, "example", page)
}

func response(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	sig_response := r.FormValue("sig_response")

	username := futuraeweb.VerifyResponse(WID, WKEY, SKEY, sig_response)
	if len(username) == 0 {
		fmt.Fprintln(w, "Could not authenticate user")
		return
	}

	fmt.Fprintf(w, "Successfully authenticated: "+username)
}

func main() {
	h := http.NewServeMux()
	h.Handle("/js/", http.StripPrefix("/js", http.FileServer(http.Dir("./"))))
	h.HandleFunc("/", handler)
	h.HandleFunc(RESPONSE_URL, response)

	fmt.Println("Listening on: http://localhost:8080/?username=example")
	http.ListenAndServe(":8080", h)
}
