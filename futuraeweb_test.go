package futuraeweb

import (
	"encoding/base64"
	"strconv"
	"strings"
	"testing"
	"time"
)

const (
	fakeWID      = "FIDXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
	wrongWID     = "FIDXXXXXXXXXXXXXXXXY"
	fakeWKEY     = "futuraegeneratedsharedwebapplicationkey."
	fakeSKEY     = "useselfgeneratedhostapplicationsecretkey"
	fakeUsername = "testusername"
)

func TestSignRequest(t *testing.T) {
	sig, err := SignRequest(fakeWID, fakeWKEY, fakeSKEY, fakeUsername)

	if sig == "" || err != nil {
		t.Error("valid signRequest failed: err=", err)
	}

	sig, err = SignRequest(fakeWID, fakeWKEY, fakeSKEY, "")
	if sig != "" || err.Error() != ErrUSER {
		t.Error("invalid user in signRequest test failed: sig=", sig, "err=", err)
	}

	sig, err = SignRequest(fakeWID, fakeWKEY, fakeSKEY, "in|valid")
	if sig != "" || err.Error() != ErrUSER {
		t.Error("invalid user in signRequest test failed: sig=", sig, "err=", err)
	}

	sig, err = SignRequest("invalid", fakeWKEY, fakeSKEY, fakeUsername)
	if sig != "" || err.Error() != ErrWID {
		t.Error("invalid wid in signRequest test failed: sig=", sig, "err=", err)
	}

	sig, err = SignRequest(fakeWID, "invalid", fakeSKEY, fakeUsername)
	if sig != "" || err.Error() != ErrWKEY {
		t.Error("invalid wkey in signRequest test failed: sig=", sig, "err=", err)
	}

	sig, err = SignRequest(fakeWID, fakeWKEY, "invalid", fakeUsername)
	if sig != "" || err.Error() != ErrSKEY {
		t.Error("invalid skey in signRequest test failed: sig=", sig, "err=", err)
	}
}

func TestVerifyResponse(t *testing.T) {
	requestSig, _ := SignRequest(fakeWID, fakeWKEY, fakeSKEY, fakeUsername)
	sigs := strings.Split(requestSig, ":")
	validAppSig := sigs[1]

	requestSig, _ = SignRequest(fakeWID, fakeWKEY, "invalidinvalidinvalidinvalidinvalidinvalid", fakeUsername)
	sigs = strings.Split(requestSig, ":")
	invalidAppSig := sigs[1]

	var tests = []struct {
		response string
		output   string
		reason   string
	}{
		{invalidResponse() + ":" + validAppSig, "", "invalid user"},
		{expiredResponse(fakeWID, fakeWKEY, fakeUsername) + ":" + validAppSig, "", "expired user"},
		{futureResponse(fakeWID, fakeWKEY, fakeUsername) + ":" + validAppSig, fakeUsername, "testusername"},
		{futureResponse(fakeWID, fakeWKEY, fakeUsername) + ":" + invalidAppSig, "", "testusername -- invalid sig"},
		{futureResponse(fakeWID, fakeWKEY, fakeUsername) + ":" + badParamsApp(fakeWID, fakeSKEY, fakeUsername), "", "verify_response - testusername, invalid app sig format"},
		{badParamsResponse(fakeWID, fakeWKEY, fakeUsername) + ":" + validAppSig, "", "verify_response - Invalid response format"},
	}

	for _, test := range tests {
		r := VerifyResponse(fakeWID, fakeWKEY, fakeSKEY, test.response)
		if r != test.output {
			t.Errorf("verify test for %s failed: got %s expected %s\n", test.reason, r, test.output)
		}
	}

	if r := VerifyResponse(wrongWID, fakeWKEY, fakeSKEY, futureResponse(fakeWID, fakeWKEY, fakeUsername)+":"+validAppSig); r != "" {
		t.Errorf("verify test for wrong WID failed: got %s expected \"\"", r)
	}
}

func invalidResponse() string {
	return "AUTH|INVALID|SIG"
}

func expiredResponse(wid, wkey, username string) string {
	expired := strconv.FormatInt(time.Now().Unix()-600, 10)
	vals := strings.Join([]string{wid, username, expired}, "|")
	cookie := cookie("AUTH", vals)
	sig := hmacSHA256(cookie, wkey)
	return strings.Join([]string{cookie, sig}, "|")
}

func futureResponse(wid, wkey, username string) string {
	expiry := strconv.FormatInt(time.Now().Unix()+600, 10)
	vals := strings.Join([]string{wid, username, expiry}, "|")
	cookie := cookie("AUTH", vals)
	sig := hmacSHA256(cookie, wkey)
	return strings.Join([]string{cookie, sig}, "|")
}

func futureEnrollResponse(wid, wkey, username string) string {
	expiry := strconv.FormatInt(time.Now().Unix()+600, 10)
	vals := strings.Join([]string{wid, username, expiry}, "|")
	cookie := cookie("ENROLL", vals)
	sig := hmacSHA256(cookie, wkey)
	return strings.Join([]string{cookie, sig}, "|")
}

func badParamsResponse(wid, wkey, username string) string {
	expiry := strconv.FormatInt(time.Now().Unix()+600, 10)
	vals := strings.Join([]string{wid, username, expiry, "bad_param"}, "|")
	cookie := cookie("AUTH", vals)
	sig := hmacSHA256(cookie, wkey)
	return strings.Join([]string{cookie, sig}, "|")
}

func badParamsApp(wid, skey, username string) string {
	expiry := strconv.FormatInt(time.Now().Unix()+600, 10)
	vals := strings.Join([]string{wid, username, expiry, "bad_param"}, "|")
	cookie := cookie("APP", vals)
	sig := hmacSHA256(cookie, skey)
	return strings.Join([]string{cookie, sig}, "|")
}

func cookie(prefix, vals string) string {
	b64 := base64.StdEncoding.EncodeToString([]byte(vals))
	return strings.Join([]string{prefix, b64}, "|")
}
